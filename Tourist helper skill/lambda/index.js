const Alexa = require('ask-sdk-core');
const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
    },
    handle(handlerInput) {
        var speakOutput = 'Welcome tourist in Delhi Hope you have a pleasant experience';
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};



const GetInfoDataHandler = {
  canHandle(handlerInput) {
    return (handlerInput.requestEnvelope.request.type === 'IntentRequest'
     && handlerInput.requestEnvelope.request.intent.name === 'GetInfoDataIntent');
  },
 async handle(handlerInput) {
    var speakOutput = 'Ask me anything else if you want';
    var spot = Alexa.getSlotValue(handlerInput.requestEnvelope, 'spot');
    var st = { spot : spot }
    var str = st.spot
    var spot1 =(
        
    str.split(' ')
   .map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
   .join(' ')

    )
    
     var mypath = 'https://your-api.herokuapp.com/api_v1/spots/?name=' + spot1;
    
    await getRemoteData(mypath)
      .then((response) => {
        var data = JSON.parse(response);
        outputSpeech = ` ${data.info}  `;
        console.log(outputSpeech);
      })
      .catch((err) => {
        console.log(`ERROR: ${err.message}`);
        
      });

    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .reprompt(speakOutput)
      .getResponse();
  },
};



const DistanceTimeHandler = {
  canHandle(handlerInput) {
    return (handlerInput.requestEnvelope.request.type === 'IntentRequest'
     && handlerInput.requestEnvelope.request.intent.name === 'DistanceTimeIntent');
  },
 async handle(handlerInput) {
    var speakOutput = 'Ask me anything else if you want';
    var spoto = Alexa.getSlotValue(handlerInput.requestEnvelope, 'spoto');
     var spotd = Alexa.getSlotValue(handlerInput.requestEnvelope, 'spotd');
    var st = { spoto : spoto , spotd : spotd }
    var str1 = st.spoto
    var str2 = st.spotd
    var mypath = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' + str1 + ',DL&destinations=' + str2 + ',DL&key=Your_API_Key';
    
    
    await getRemoteData(mypath)
      .then((response) => {
        var data = JSON.parse(response);
        outputSpeech = ` The distance between the two places are ${data.rows[0].elements[0].distance.text} and the time taken to travel is ${data.rows[0].elements[0].duration.text}  `;
      })
      .catch((err) => {
        console.log(`ERROR: ${err.message}`);
       
      });

    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .reprompt(speakOutput)
      .getResponse();
  },
};


const GetThingsToDoHandler = {
  canHandle(handlerInput) {
    return (handlerInput.requestEnvelope.request.type === 'IntentRequest'
     && handlerInput.requestEnvelope.request.intent.name === 'GetThingsToDoIntent');
  },
 async handle(handlerInput) {
    var speakOutput = 'Ask me anything else if you want';
    var spot = Alexa.getSlotValue(handlerInput.requestEnvelope, 'spot');
    var st = { spot : spot }
    var str = st.spot
    var spot1 =(

    str.split(' ')
   .map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
   .join(' ')

    )
    var mypath = 'https://your-api.herokuapp.com/api_v1/spots/?name=' + spot1;
    
    
    await getRemoteData(mypath)
      .then((response) => {
        var data = JSON.parse(response);
        outputSpeech = ` ${data.things_to_do}  `;
      })
      .catch((err) => {
        console.log(`ERROR: ${err.message}`);
        
      });

    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .reprompt(speakOutput)
      .getResponse();
  },
};





const GetLocationHandler = {
  canHandle(handlerInput) {
    return (handlerInput.requestEnvelope.request.type === 'IntentRequest'
     && handlerInput.requestEnvelope.request.intent.name === 'GetLocationIntent');
  },
 async handle(handlerInput) {
    var speakOutput = 'Ask me anything else if you want';
    var spot = Alexa.getSlotValue(handlerInput.requestEnvelope, 'spot');
    var st = { spot : spot }
    var str = st.spot
    var spot1 =(

    str.split(' ')
   .map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
   .join(' ')

    )
    var mypath = 'https://your-api.herokuapp.com/api_v1/spots/?name=' + spot1;
    
    
    await getRemoteData(mypath)
      .then((response) => {
        var data = JSON.parse(response);
        outputSpeech = ` ${ data.location }  `;
      })
      .catch((err) => {
        console.log(`ERROR: ${err.message}`);
        
      });

    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .reprompt(speakOutput)
      .getResponse();
  },
};



const GetTimeToVisitHandler = {
  canHandle(handlerInput) {
    return (handlerInput.requestEnvelope.request.type === 'IntentRequest'
     && handlerInput.requestEnvelope.request.intent.name === 'GetTimeToVisitIntent');
  },
 async handle(handlerInput) {
    var speakOutput = 'Ask me anything else if you want';
    var spot = Alexa.getSlotValue(handlerInput.requestEnvelope, 'spot');
    var st = { spot : spot }
    var str = st.spot
    var spot1 =(

    str.split(' ')
   .map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
   .join(' ')

    )
    const mypath = 'https://your-api.herokuapp.com/api_v1/spots/?name=' + spot1;
    
    
    await getRemoteData(mypath)
      .then((response) => {
        var data = JSON.parse(response);
        outputSpeech = ` ${data.time_to_visit}  `;
      })
      .catch((err) => {
        console.log(`ERROR: ${err.message}`);
       
      });

    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .reprompt(speakOutput)
      .getResponse();
  },
};






const GetSimilarPlacesHandler = {
  canHandle(handlerInput) {
    return (handlerInput.requestEnvelope.request.type === 'IntentRequest'
     && handlerInput.requestEnvelope.request.intent.name === 'GetSimilarPlacesIntent');
  },
 async handle(handlerInput) {
    var speakOutput = 'Ask me anything else if you want';
    var spot = Alexa.getSlotValue(handlerInput.requestEnvelope, 'spot');
    var st = { spot : spot }
    var str = st.spot
    var spot1 =(

    str.split(' ')
   .map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
   .join(' ')

    )
    const mypath = 'https://your-api.herokuapp.com/api_v1/spots/?name=' + spot1;
    
    
    await getRemoteData(mypath)
      .then((response) => {
        var data = JSON.parse(response);
        outputSpeech = ` ${data.similar_places}  `;
      })
      .catch((err) => {
        console.log(`ERROR: ${err.message}`);
       
      });

    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .reprompt(speakOutput)
      .getResponse();
  },
};






const  GetNearByPlacesHandler = {
  canHandle(handlerInput) {
    return (handlerInput.requestEnvelope.request.type === 'IntentRequest'
     && handlerInput.requestEnvelope.request.intent.name === 'GetNearByPlacesIntent');
  },
 async handle(handlerInput) {
    var speakOutput = 'Ask me anything else if you want';
    var spot = Alexa.getSlotValue(handlerInput.requestEnvelope, 'spot');
    var st = { spot : spot }
    var str = st.spot
    var spot1 =(

    str.split(' ')
   .map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
   .join(' ')

    )
    var mypath = 'https://your-api.herokuapp.com/api_v1/spots/?name=' + spot1;
    
    
    await getRemoteData(mypath)
      .then((response) => {
        var data = JSON.parse(response);
        outputSpeech = ` ${data.near_by_places}  `;
      })
      .catch((err) => {
        console.log(`ERROR: ${err.message}`);
       
      });

    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .reprompt(speakOutput)
      .getResponse();
  },
};



const GetSpecialAttractionHandler = {
  canHandle(handlerInput) {
    return (handlerInput.requestEnvelope.request.type === 'IntentRequest'
     && handlerInput.requestEnvelope.request.intent.name === 'GetSpecialAttractionIntent');
  },
 async handle(handlerInput) {
    var speakOutput = 'Ask me anything else if you want';
    var spot = Alexa.getSlotValue(handlerInput.requestEnvelope, 'spot');
    var st = { spot : spot }
    var str = st.spot
    var spot1 =(

    str.split(' ')
   .map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
   .join(' ')

    )
    var mypath = 'https://your-api.herokuapp.com/api_v1/spots/?name=' + spot1;
    
    
    await getRemoteData(mypath)
      .then((response) => {
        var data = JSON.parse(response);
        outputSpeech = ` ${data.special_attraction}  `;
      })
      .catch((err) => {
        console.log(`ERROR: ${err.message}`);
       
      });

    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .reprompt(speakOutput)
      .getResponse();
  },
};










const HelpIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    const speechText = 'You can introduce yourself by telling me your name';

    return handlerInput.responseBuilder
      .speak(speechText)
      .getResponse();
  },
};




const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
        || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
  },
  handle(handlerInput) {
    const speechText = 'Goodbye!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .getResponse();
  },
};





const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

    return handlerInput.responseBuilder.getResponse();
  },
};







const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);

    return handlerInput.responseBuilder
      .speak('Sorry, I can\'t understand the command. Please say again.')
      .reprompt('Sorry, I can\'t understand the command. Please say again.')
      .getResponse();
  },
};






const getRemoteData = (url) => new Promise((resolve, reject) => {
  const client = url.startsWith('https') ? require('https') : require('http');
  const request = client.get(url, (response) => {
    if (response.statusCode < 200 || response.statusCode > 299) {
      reject(new Error(`Failed with status code: ${response.statusCode}`));
    }
    const body = [];
    response.on('data', (chunk) => body.push(chunk));
    response.on('end', () => resolve(body.join('')));
  });
  request.on('error', (err) => reject(err));
});





const skillBuilder = Alexa.SkillBuilders.custom();









exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    GetSpecialAttractionHandler,
    DistanceTimeHandler,
    GetNearByPlacesHandler,
    GetSimilarPlacesHandler,
    GetTimeToVisitHandler,
    GetLocationHandler,
    GetInfoDataHandler,
    GetThingsToDoHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    SessionEndedRequestHandler,
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();
